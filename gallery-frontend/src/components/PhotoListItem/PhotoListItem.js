import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

const PhotoListItem = props => {
    return (
        <Panel>
          <Panel.Body>
            <Image
                style={{width: '100px', marginRight: '10px'}}
                src={config.apiUrl + '/uploads/' + props.image}
                thumbnail
            />

            <Link to={'/photos/' + props.photoId}>
                {props.title}
            </Link>

            <Link to={'/photos/' + props.userId}>
              <strong style={{marginLeft: '10px'}}>
                By: {props.displayName}
              </strong>
            </Link>
          </Panel.Body>
        </Panel>
    );
};

PhotoListItem.propTypes = {
    photoId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired
};

export default PhotoListItem;