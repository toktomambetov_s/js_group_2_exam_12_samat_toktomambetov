import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";

import {Link} from "react-router-dom";

import PhotoListItem from '../../components/PhotoListItem/PhotoListItem';
import {fetchPhotos} from "../../store/actions/photos";

class Photos extends Component {
    componentDidMount() {
        this.props.onFetchPhotos();
    }

    render() {
        return (
            <Fragment>
              <PageHeader>
                Photo Gallery
                  {this.props.user && this.props.user.role === 'user' &&
                  <Link to="/photos/new">
                    <Button bsStyle="primary" className="pull-right">
                      Add new photo
                    </Button>
                  </Link>
                  }
              </PageHeader>

                {this.props.photos.map(photo => (
                    <PhotoListItem
                        key={photo._id}
                        photoId={photo._id}
                        title={photo.title}
                        image={photo.image}
                        userId={photo.author._id}
                        displayName={photo.author.displayName}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        photos: state.photos.photos,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPhotos: () => dispatch(fetchPhotos())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Photos);