import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import PageHeader from "react-bootstrap/es/PageHeader";
import PhotoListItem from "../../components/PhotoListItem/PhotoListItem";
import {fetchUserPhotos} from "../../store/actions/photos";

class UserPhotos extends Component {
    componentDidMount() {
        this.props.onFetchUserPhotos();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    John Doe's Gallery
                </PageHeader>

                {this.props.userPhotos.map(photo => (
                    <PhotoListItem
                        key={photo._id}
                        photoId={photo._id}
                        title={photo.title}
                        image={photo.image}
                        userId={photo.author._id}
                        displayName={photo.author.displayName}
                    />
                ))}
            </Fragment>
        )
    }
};

const mapStateToProps = state => ({
    userPhotos: state.photos.userPhotos
});

const mapDispatchToProps = dispatch => ({
    onFetchUserPhotos: () => dispatch(fetchUserPhotos())
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPhotos);