import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Photos from "./containers/Photos/Photos";
import Login from "./containers/Login/Login";
import NewPhoto from './containers/NewPhoto/NewPhoto';
import UserPhotos from "./containers/UserPhotos/UserPhotos";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = ({user}) => (
    <Switch>
        <Route path="/" exact component={Photos}/>
        <ProtectedRoute
            isAllowed={user && user.role === 'user'}
            path="/photos/new"
            exact
            component={NewPhoto}
        />
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/photos/:id" render={(props) => (
            <UserPhotos {...props} />)}
               exact component={UserPhotos}
        />
    </Switch>
);

export default Routes;