import axios from '../../axios-api';
import {push} from "react-router-redux";
import {CREATE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS, FETCH_USER_PHOTOS_SUCCESS} from "./actionTypes";

export const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos}
};

export const fetchPhotos = () => {
    return dispatch => {
        axios.get('/photos').then(
            response => dispatch(fetchPhotosSuccess(response.data))
        );
    }
};

export const createPhotoSuccess = () => {
    return {type: CREATE_PHOTO_SUCCESS};
};

export const createPhoto = photoData => {
    return dispatch => {
        return axios.post('/photos/:id/publish', photoData).then(
            response => {
                dispatch(createPhotoSuccess());
                dispatch(push('/'))
            }
        );
    };
};

export const fetchUserPhotosSuccess = photos => {
    return {type: FETCH_USER_PHOTOS_SUCCESS, photos}
};

export const fetchUserPhotos = (id) => {
    return dispatch => {
        axios.get('/photos/:id' + id).then(
            response => dispatch(fetchUserPhotos(response.data))
        )
    }
};