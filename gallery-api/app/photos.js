const express = require('express');
const multer = require('multer');
const path = require('path');
const ObjectId = require('mongodb').ObjectId;
const nanoid = require('nanoid');
const Photo = require('../models/Photo');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Photo.find().populate('author')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;

        Photo.find({author: new ObjectId(id)}).populate('author')
            .then(results => {
                if (results) res.send(results);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.post('/:id/publish', upload.single('image'), (req, res) => {
        console.log(req.body);
        const photoData = req.body;

        const photo = new Photo(photoData);

        photo.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;