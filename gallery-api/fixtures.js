const mongoose = require('mongoose');
const config = require('./config');

const Photo = require('./models/Photo');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('photos');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [user, admin] = await User.create({
        username: 'user',
        password: '123',
        role: 'user',
        displayName: 'John Doe'
    }, {
        username: 'admin',
        password: 'admin123',
        role: 'admin',
        displayName: 'John Doe'
    });

    await Photo.create({
        title: 'Beautiful nature',
        image: 'nature.jpg',
        author: user._id
    }, {
        title: 'My second post',
        image: 'nature2.jpg',
        author: user._id
    });

    db.close();
});