const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'gallery'
  },
  jwt: {
    secret: 'some kinda very secret string',
    expires: 3600
  },
  facebook: {
    appId: "434044487067223",
    appSecret: "eab0635775a0dd40b75f7b2d3888cf7f"
  }
};

